// TODO: render danh sách món ăn từ array 

function renderMonAn(arr) {
    var contentHTML = "" ; 
    for (var i = 0; i < arr.length; i++) {
        var contentTr =
          `
         <tr>
            <td>${arr[i].id}</td>
            <td>${arr[i].tenMon}</td>
            <td>${arr[i].giaMon}</td>
            <td>${arr[i].hinhAnh}</td>
            <td>${arr[i].loaiMon}</td>
            <td><button onclick = "xoaMonAn(${arr[i].id})" class ="btn btn-danger mr-3" >Delete</button><button onclick = "suaMonAn(${arr[i].id})" class ="btn btn-primary" >Edit</button></td>
         </tr>
        `
        contentHTML += contentTr;
    }
    document.getElementById('danhSachMonAn').innerHTML = contentHTML;
}

// SHOW THONG TIN RA NGOAI 

function showThongTin() {
    // TODO: Xây dựng cấu trúc axios 
    axios({
        url : "https://63bea7fae348cb0762149a43.mockapi.io/list__food" , 
        method : "GET" , 
    })
    .then(function (res) {
        renderMonAn(res.data) ; 
    })
    .catch(function (err) {
        console.log(err);
    });
    
}

function lamMoiForm() {
    document.getElementById('tenMon').value = " " ;  
    document.getElementById('giaMon').value = " " ; 
    document.getElementById('hinhAnh').value = " " ;  
    document.getElementById('loaiMon').value = " " ; 
}

