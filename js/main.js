// TODO: render dữ liệu ra ngoài 
showThongTin() ;


// TODO: Xây dựng chức năng thêm món ăn 

function btnThemMonAn() {
    axios({
        url : `https://63bea7fae348cb0762149a43.mockapi.io/list__food`,  
        method : "POST", 
        data : {
            tenMon :  document.getElementById('tenMon').value , 
            giaMon :  document.getElementById('giaMon').value,
            hinhAnh : document.getElementById('hinhAnh').value , 
            loaiMon :  document.getElementById('loaiMon').value
        }
    })
    .then(function () {
        showThongTin() ;
        lamMoiForm() ; 
    })
    .catch(function (err){
        console.log(err) ;
    })         
}


// TODO: Xây dựng chức năng xóa dữ liệu 

function xoaMonAn(id) {
    axios({
        url : `https://63bea7fae348cb0762149a43.mockapi.io/list__food/${id}` ,  
        method : "DELETE" , 
    })
    .then(function(){
        showThongTin() ; 
    })
    .catch(function(err){
        console.log(err) ;
    })
}

// TODO: xây dựng chức năng sửa thông tin món ăn 

function suaMonAn(id) {
    axios({
        url : `https://63bea7fae348cb0762149a43.mockapi.io/list__food/${id}`,  
        method : "GET" ,
    })
    .then(function(res){
        document.getElementById('tenMon').value = res.data.tenMon ;
        document.getElementById('giaMon').value = res.data.giaMon ;
        document.getElementById('hinhAnh').value = res.data.hinhAnh
        document.getElementById('loaiMon').value = res.data.loaiMon ;
        document.getElementById('identify').value = res.data.id ; 
        var capNhatEl = document.getElementById('capNhat') ; 
        capNhatEl.style.display = "inline-block" ; 
    })
    .catch(function(err){
        console.log(err) ;
    })

}

// TODO: xây dựng chức năng cập nhật thông tin món ăn 

function capNhatMonAn() {
    var idEl = document.getElementById('identify').value ; 
    axios({
        url : `https://63bea7fae348cb0762149a43.mockapi.io/list__food/${idEl}`,  
        method : "PUT" , 
        data : {
            tenMon :  document.getElementById('tenMon').value,
            giaMon :  document.getElementById('giaMon').value,
            hinhAnh : document.getElementById('hinhAnh').value,
            loaiMon :  document.getElementById('loaiMon').value
        }    
    })
    .then(function(){
        showThongTin() ; 
        lamMoiForm() ;
        var capNhatEl = document.getElementById('capNhat') ;
        capNhatEl.style.display = "none" ;
    })
    .catch(function(err){
        console.log(err) ;
    })
}



